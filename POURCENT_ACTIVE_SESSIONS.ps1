﻿$returnOK = 0;
$returnWarning = 1;
$returnCrit = 2;
$returnUnknow = 3;




Try {
    $UserSession = Get-RDUserSession;
    $nbTotSession = $UserSession.Count;
    $nbActiveSession = 0;
    Foreach ($session in $UserSession){
        If ($session.SessionState -eq "STATE_ACTIVE"){
            $nbActiveSession ++;
        }
    }
    $pourcent = 100- ($nbActiveSession/$nbTotSession*100);
            If ($pourcent -gt $args[0])
            {
                $returnValue = 2;
            }
            else {
                $returnValue = 0;
            }
}
Catch {
    $returnValue = 3;
}
Finally {
    If ($returnValue -eq 3){
        Write-host "Unknow state : Possible script or argument error"
        exit $returnUnknow
    }
    else {
        if ($returnValue -eq 2) {
            Write-host "Critical state : $pourcent % opened and not active session(s)"
            exit $returnCrit
        }
        else {
            Write-host "OK : $pourcent % opened and not active session(s)"
            exit $returnOK
        }
    }
}

