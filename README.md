# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This plugin allows Icinga to check the status of RDS / RDP use in Windows architecture. 

* Version

1.0

### How do I get set up? ###

* Summary of set up

This plugin interconnect Icinga and Windows RDS so you need to have both installed. 

* Configuration



* Dependencies

To execute the PowerShell scripts that are provided here, you need to have NSClient++ installed on the Windows server. 

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Camille Pernoud
camille.pernoud.2001@gmail.com
camille.pernoud.free.fr

* Other community or team contact