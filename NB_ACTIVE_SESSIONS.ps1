﻿$returnOK = 0;
$returnWarning = 1;
$returnCrit = 2;
$returnUnknow = 3;




Try {
    $UserSession = Get-RDUserSession;
    $nbActiveSession = 0;
    Foreach ($session in $UserSession){
        If ($session.SessionState -eq "STATE_ACTIVE"){
            $nbActiveSession ++;
        }
    }
            If ($nbActiveSession -gt $args[0])
            {
                $returnValue = 2;
            }
            else {
                $returnValue = 0;
            }
}
Catch {
    $returnValue = 3;
}
Finally {
    If ($returnValue -eq 3){
        Write-host "Unknow state : Possible script or argument error"
        exit $returnUnknow
    }
    else {
        if ($returnValue -eq 2) {
            Write-host "Critical state : $nbActiveSession session(s) opened"
            exit $returnCrit
        }
        else {
            Write-host "OK : $nbActiveSession session(s) opened"
            exit $returnOK
        }
    }
}

